use anyhow::*;
use spirv_builder::{Capability, MetadataPrintout, SpirvBuilder};

use std::thread;

fn build_vox_spir() -> Result<()> {
    SpirvBuilder::new("./vox-spir", "spirv-unknown-vulkan1.1")
        .release(true)
        .capability(Capability::Int8)
        .capability(Capability::Int16)
        .capability(Capability::Int64)
        .capability(Capability::RuntimeDescriptorArray)
        .extension("SPV_EXT_descriptor_indexing")
        .uniform_buffer_standard_layout(true)
        .print_metadata(MetadataPrintout::Full)
        .build()?;
    Ok(())
}

fn build_vox_compute() -> Result<()> {
    SpirvBuilder::new("./vox-compute", "spirv-unknown-vulkan1.1")
        .release(true)
        .capability(Capability::Int8)
        .capability(Capability::Int16)
        .capability(Capability::Int64)
        .capability(Capability::RuntimeDescriptorArray)
        .extension("SPV_EXT_descriptor_indexing")
        .uniform_buffer_standard_layout(true)
        .print_metadata(MetadataPrintout::Full)
        .build()?;
    Ok(())
}

fn main() -> Result<()> {
    let spir = thread::spawn(build_vox_spir);
    let compute = thread::spawn(build_vox_compute);

    spir.join().unwrap()?;
    compute.join().unwrap()?;

    Ok(())
}
