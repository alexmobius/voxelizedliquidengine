use bytemuck::{bytes_of, Pod};
use core::mem::size_of;
use core::sync::atomic::AtomicBool;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::sync::atomic::Ordering;
use wgpu::{
    BindingResource, BindingType, Buffer, BufferAddress, BufferBindingType, BufferDescriptor,
    BufferSize, BufferUsages, CommandEncoder, Device, MapMode,
};

pub struct CPUOwnedData<T: Pod> {
    inner: T,
    buffer: Buffer,
    dirty: AtomicBool,
}

pub struct CPUOwnedDataGuard<'a, T: Pod> {
    reference: &'a mut T,
    dirty: &'a AtomicBool,
}

impl<T: Pod> CPUOwnedData<T> {
    pub fn new(inner: T, device: &Device, label: &str) -> Self {
        let buffer = device.create_buffer(&BufferDescriptor {
            label: Some(label),
            size: size_of::<T>() as u64,
            usage: BufferUsages::STORAGE | BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
        Self {
            inner,
            buffer,
            dirty: AtomicBool::new(true),
        }
    }

    pub fn refresh_ordered(&self, device: &Device, encoder: &mut CommandEncoder) {
        if !self.dirty.swap(false, Ordering::Acquire) {
            return;
        }
        let staging = device.create_buffer(&BufferDescriptor {
            label: None,
            size: size_of::<T>() as BufferAddress,
            usage: BufferUsages::MAP_WRITE | BufferUsages::COPY_SRC,
            mapped_at_creation: true,
        });
        let mut range = staging.slice(..).get_mapped_range_mut();
        range.copy_from_slice(bytes_of(&self.inner));
        drop(range);
        staging.unmap();
        encoder.copy_buffer_to_buffer(
            &staging,
            0,
            &self.buffer,
            0,
            size_of::<T>() as BufferAddress,
        );
    }

    pub fn lock(&mut self) -> CPUOwnedDataGuard<T> {
        CPUOwnedDataGuard {
            reference: &mut self.inner,
            dirty: &self.dirty,
        }
    }

    pub fn binding_descriptor(&self) -> BindingType {
        BindingType::Buffer {
            ty: BufferBindingType::Storage { read_only: true },
            has_dynamic_offset: false,
            min_binding_size: BufferSize::new(size_of::<T>() as u64),
        }
    }

    pub fn binding(&self) -> BindingResource {
        self.buffer.as_entire_binding()
    }
}

impl<T: Pod> AsRef<T> for CPUOwnedData<T> {
    fn as_ref(&self) -> &T {
        return &self.inner;
    }
}

impl<'a, T: Pod> Deref for CPUOwnedDataGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.reference
    }
}

impl<'a, T: Pod> DerefMut for CPUOwnedDataGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.reference
    }
}

impl<'a, T: Pod> Drop for CPUOwnedDataGuard<'a, T> {
    fn drop(&mut self) {
        self.dirty.store(true, Ordering::Release);
    }
}

pub struct GPUOwnedData<T: Pod> {
    buffer: Buffer,
    _marker: PhantomData<*const T>,
}

impl<T: Pod> GPUOwnedData<T> {
    pub fn new(device: &Device, label: &str) -> Self {
        let buffer = device.create_buffer(&BufferDescriptor {
            label: Some(label),
            size: size_of::<T>() as u64,
            usage: BufferUsages::STORAGE | BufferUsages::MAP_READ,
            mapped_at_creation: false,
        });
        Self {
            buffer,
            _marker: PhantomData,
        }
    }

    pub async fn map(&self) -> T {
        let map_slice = self.buffer.slice(..);
        map_slice.map_async(MapMode::Read).await.unwrap();
        let result = *bytemuck::from_bytes::<T>(&map_slice.get_mapped_range());
        self.buffer.unmap();
        result
    }

    pub fn binding_descriptor(&self) -> BindingType {
        BindingType::Buffer {
            ty: BufferBindingType::Storage { read_only: false },
            has_dynamic_offset: false,
            min_binding_size: BufferSize::new(size_of::<T>() as u64),
        }
    }

    pub fn binding(&self) -> BindingResource {
        self.buffer.as_entire_binding()
    }
}
