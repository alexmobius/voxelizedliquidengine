use wgpu::{
    Device, RenderPassDescriptor, TextureDescriptor, TextureDimension, TextureFormat,
    TextureUsages, TextureView,
};
use winit::dpi::PhysicalSize;

use crate::texture::Texture;

struct Target {
    texture: Texture,
    debug_show: Option<DebugShow>,
}

pub struct Targets {
    size: winit::dpi::PhysicalSize<u32>,
    normal: Target,
    cell: Target,
    offset: Target,
    z_buffer: Target,
    depth_stencil: Target,
}

#[derive(Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum DebugShow {
    None,
    Normal,
    Offset,
    RayDir,
}

impl Target {
    fn new(
        device: &Device,
        debug_show: Option<DebugShow>,
        desc: TextureDescriptor<'static>,
    ) -> Self {
        let texture = Texture::new(device, desc);
        Self {
            texture,
            debug_show,
        }
    }

    fn or<'a>(&'a self, debug: DebugShow, other: &'a TextureView) -> &'a TextureView {
        if self.debug_show == Some(debug) {
            other
        } else {
            self.texture.as_ref()
        }
    }
}

impl Targets {
    pub fn new(size: PhysicalSize<u32>, device: &Device) -> Self {
        let extent = wgpu::Extent3d {
            width: size.width,
            height: size.height,
            depth_or_array_layers: 1,
        };
        let normal = Target::new(
            device,
            Some(DebugShow::Normal),
            TextureDescriptor {
                label: Some("normal buffer"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba8Snorm,
                usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
            },
        );
        let cell = Target::new(
            device,
            None,
            TextureDescriptor {
                label: Some("cell idx"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba32Sint,
                usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
            },
        );
        let offset = Target::new(
            device,
            Some(DebugShow::Offset),
            TextureDescriptor {
                label: Some("offset"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba8Unorm,
                usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
            },
        );
        let z_buffer = Target::new(
            device,
            None,
            TextureDescriptor {
                label: Some("z-buffer"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::R32Float,
                usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
            },
        );
        let depth_stencil = Target::new(
            device,
            None,
            TextureDescriptor {
                label: Some("depth stencil"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Depth24PlusStencil8,
                usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
            },
        );
        Self {
            size,
            normal,
            cell,
            offset,
            z_buffer,
            depth_stencil,
        }
    }

    fn canonical_color_targets(&self) -> [&Target; 4] {
        [&self.normal, &self.cell, &self.offset, &self.z_buffer]
    }

    pub fn color_target_states(
        &self,
        debug: DebugShow,
        output_format: TextureFormat,
    ) -> [wgpu::ColorTargetState; 4] {
        self.canonical_color_targets().map(|tar| {
            if tar.debug_show == Some(debug) {
                output_format.into()
            } else {
                tar.texture.color_target_state()
            }
        })
    }

    pub fn begin_render_pass<'a>(
        &'a self,
        encoder: &'a mut wgpu::CommandEncoder,
        debug: DebugShow,
        output: &'a TextureView,
    ) -> wgpu::RenderPass<'a> {
        let attachments: heapless::Vec<_, 4> = self
            .canonical_color_targets()
            .iter()
            .map(|&target| wgpu::RenderPassColorAttachment {
                view: target.or(debug, output),
                resolve_target: None,
                ops: Default::default(),
            })
            .collect();

        encoder.begin_render_pass(&RenderPassDescriptor {
            label: None,
            color_attachments: &attachments,
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: self.depth_stencil.texture.as_ref(),
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(1.0),
                    store: true,
                }),
                stencil_ops: None,
            }),
        })
    }
}
