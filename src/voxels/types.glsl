#extension GL_EXT_shader_image_load_formatted : require
#extension GL_EXT_samplerless_texture_functions : require

const int MAX_ATLAS = 5;
const int MAX_CHUNKS = 64;

layout(binding=0, set=0, std430) buffer VoxelInfo {
    uint num_chunks;
    ivec3 chunk_size;
    uint chunk_flags[MAX_CHUNKS];
    ivec3 worldOrigin[MAX_CHUNKS];
    ivec3 atlasOrigin[MAX_CHUNKS];
} voxel_info;
layout(binding=1, set=0) uniform texture3D atlasIn[MAX_ATLAS];
layout(binding=2, set=0) uniform image3D atlasOut[MAX_ATLAS];
layout(binding=3, set=0) uniform sampler nearestSampler;
layout(binding=4, set=0) uniform sampler bilinearSampler;

