use crate::voxel_data::Chunks;
use std::ops::Range;
use vox_populi::RayTracingGlobalData;
use wgpu::{
    include_spirv_raw, BindGroupLayout, ColorTargetState, CompareFunction, ComputePipeline,
    ComputePipelineDescriptor, DepthBiasState, DepthStencilState, Device, Face, FragmentState,
    FrontFace, PipelineLayout, PipelineLayoutDescriptor, PrimitiveState, PrimitiveTopology,
    PushConstantRange, RenderPipeline, RenderPipelineDescriptor, ShaderModule, ShaderStages,
    StencilFaceState, StencilOperation, StencilState, TextureFormat, VertexState,
};

pub struct Shaders {
    pub vox_spir: wgpu::ShaderModule,
    pub vox_compute: wgpu::ShaderModule,
    compute_atlas_layout: PipelineLayout,
    pub generate_pipeline: ComputePipeline,
    render_pipeline_layout: PipelineLayout,
    pub render_pipeline: RenderPipeline,
    apply_ordering_layout: PipelineLayout,
    pub apply_ordering_pipeline: ComputePipeline,
    color_target_states: heapless::Vec<ColorTargetState, 4>,
}

fn load(device: &Device, desc: &wgpu::ShaderModuleDescriptorSpirV) -> wgpu::ShaderModule {
    unsafe { device.create_shader_module_spirv(desc) }
}
impl Shaders {
    pub fn new(device: &Device, chunks: &Chunks, targets: &[ColorTargetState]) -> Self {
        let vox_spir = load(device, &include_spirv_raw!(env!("vox_spir.spv")));
        let vox_compute = load(device, &include_spirv_raw!(env!("vox_compute.spv")));
        let compute_atlas_layout = create_compute_atlas_layout(device, &chunks.bind_layout);
        let generate_pipeline =
            create_generate_pipeline(device, &compute_atlas_layout, &vox_compute);
        let render_pipeline_layout = create_pipeline_layout(device, chunks);
        let render_pipeline =
            create_render_pipeline(device, &render_pipeline_layout, &vox_spir, targets);
        let apply_ordering_layout = create_apply_ordering_pipeline_layout(device, chunks);
        let apply_ordering_pipeline =
            create_apply_ordering_pipeline(device, &apply_ordering_layout, &vox_compute);
        Self {
            vox_spir,
            vox_compute,
            compute_atlas_layout,
            generate_pipeline,
            render_pipeline_layout,
            render_pipeline,
            apply_ordering_layout,
            apply_ordering_pipeline,
            color_target_states: targets.iter().cloned().collect(),
        }
    }
}

fn create_compute_atlas_layout(device: &Device, layout: &BindGroupLayout) -> PipelineLayout {
    let desc = PipelineLayoutDescriptor {
        label: Some("atlas compute layout"),
        bind_group_layouts: &[layout],
        push_constant_ranges: &[PushConstantRange {
            stages: ShaderStages::COMPUTE,
            range: 0..(std::mem::size_of::<Range<u32>>() as u32),
        }],
    };
    device.create_pipeline_layout(&desc)
}

fn create_pipeline_layout(device: &Device, chunks: &Chunks) -> PipelineLayout {
    let desc = PipelineLayoutDescriptor {
        label: Some("default layout"),
        bind_group_layouts: &[&chunks.bind_layout],
        push_constant_ranges: &[PushConstantRange {
            stages: ShaderStages::VERTEX_FRAGMENT,
            range: 0..(std::mem::size_of::<RayTracingGlobalData>() as u32),
        }],
    };
    device.create_pipeline_layout(&desc)
}

fn create_generate_pipeline(
    device: &Device,
    layout: &PipelineLayout,
    vox_compute: &ShaderModule,
) -> ComputePipeline {
    let desc = ComputePipelineDescriptor {
        label: Some("generation"),
        layout: Some(layout),
        module: &vox_compute,
        entry_point: "generate::sponge",
    };
    device.create_compute_pipeline(&desc)
}

fn create_apply_ordering_pipeline_layout(device: &Device, chunks: &Chunks) -> PipelineLayout {
    let desc = PipelineLayoutDescriptor {
        label: Some("apply ordering pipeline"),
        bind_group_layouts: &[&chunks.bind_layout, &chunks.ordering_bind_layout],
        push_constant_ranges: &[],
    };
    device.create_pipeline_layout(&desc)
}

fn create_apply_ordering_pipeline(
    device: &Device,
    layout: &PipelineLayout,
    vox_compute: &ShaderModule,
) -> ComputePipeline {
    let desc = ComputePipelineDescriptor {
        label: Some("apply ordering"),
        layout: Some(layout),
        module: &vox_compute,
        entry_point: "sort::apply_ordering",
    };
    device.create_compute_pipeline(&desc)
}

fn create_render_pipeline(
    device: &Device,
    layout: &PipelineLayout,
    vox_spir: &ShaderModule,
    targets: &[ColorTargetState],
) -> RenderPipeline {
    let desc = RenderPipelineDescriptor {
        label: Some("raytracing"),
        layout: Some(layout),
        vertex: VertexState {
            module: &vox_spir,
            entry_point: "block_cuboid",
            buffers: &[],
        },
        primitive: PrimitiveState {
            topology: PrimitiveTopology::TriangleList,
            strip_index_format: None,
            cull_mode: Some(Face::Back),
            front_face: FrontFace::Ccw,
            conservative: true,
            unclipped_depth: true,
            ..Default::default()
        },
        depth_stencil: Some(DepthStencilState {
            format: TextureFormat::Depth24PlusStencil8,
            depth_write_enabled: true,
            depth_compare: CompareFunction::Less,
            stencil: Default::default(),
            bias: Default::default(),
        }),
        multisample: Default::default(),
        fragment: Some(FragmentState {
            module: &vox_spir,
            entry_point: "raytrace",
            targets,
        }),
        multiview: None,
    };
    device.create_render_pipeline(&desc)
}
