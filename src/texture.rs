#[derive(Debug)]
pub(crate) struct Texture {
    desc: wgpu::TextureDescriptor<'static>,
    pub(crate) texture: wgpu::Texture,
    default_view: wgpu::TextureView,
}

impl Texture {
    pub(crate) fn new(device: &wgpu::Device, desc: wgpu::TextureDescriptor<'static>) -> Self {
        let texture = device.create_texture(&desc);
        let default_view = texture.create_view(&Default::default());
        Self {
            desc,
            texture,
            default_view,
        }
    }

    pub(crate) fn color_target_state(&self) -> wgpu::ColorTargetState {
        self.desc.format.into()
    }

    pub(crate) fn layout_entry_ro(&self, binding: u32) -> wgpu::BindGroupLayoutEntry {
        use wgpu::{
            BindGroupLayoutEntry, BindingType, ShaderStages, TextureDimension, TextureViewDimension,
        };
        let info = self.desc.format.describe();
        BindGroupLayoutEntry {
            binding,
            visibility: ShaderStages::VERTEX_FRAGMENT | ShaderStages::COMPUTE,
            ty: BindingType::Texture {
                view_dimension: match self.desc.dimension {
                    TextureDimension::D1 => TextureViewDimension::D1,
                    TextureDimension::D2 => TextureViewDimension::D2,
                    TextureDimension::D3 => TextureViewDimension::D3,
                },
                sample_type: info.sample_type,
                multisampled: self.desc.sample_count != 1,
            },
            count: None,
        }
    }

    pub(crate) fn layout_entry_wo(&self, binding: u32) -> wgpu::BindGroupLayoutEntry {
        use wgpu::{
            BindGroupLayoutEntry, BindingType, ShaderStages, StorageTextureAccess,
            TextureDimension, TextureViewDimension,
        };
        BindGroupLayoutEntry {
            binding,
            visibility: ShaderStages::VERTEX_FRAGMENT | ShaderStages::COMPUTE,
            ty: BindingType::StorageTexture {
                access: StorageTextureAccess::WriteOnly,
                format: self.desc.format,
                view_dimension: match self.desc.dimension {
                    TextureDimension::D1 => TextureViewDimension::D1,
                    TextureDimension::D2 => TextureViewDimension::D2,
                    TextureDimension::D3 => TextureViewDimension::D3,
                },
            },
            count: None,
        }
    }
}

impl AsRef<wgpu::TextureView> for Texture {
    fn as_ref(&self) -> &wgpu::TextureView {
        &self.default_view
    }
}
