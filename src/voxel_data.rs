use crate::buffer::{CPUOwnedData, GPUOwnedData};
use crate::texture::Texture;
use bytemuck::Zeroable;
use glam::{IVec3, UVec3};
use heapless::Vec as HeaplessVec;
use std::num::NonZeroU32;
use std::ops::Range;
use vox_populi::chunks::{
    ChunkAuxData, ChunkFound, ChunkLibrary, Ordering, CHUNK_SIZE, MAX_ATLAS, MAX_CHUNKS,
};
use wgpu::{
    BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout, BindGroupLayoutDescriptor,
    BindGroupLayoutEntry, BindingResource, BindingType, CommandEncoder, CommandEncoderDescriptor,
    ComputePassDescriptor, ComputePipeline, Device, Extent3d, ImageCopyTexture, Origin3d, Queue,
    ShaderStages, StorageTextureAccess, TextureAspect, TextureDescriptor, TextureDimension,
    TextureFormat, TextureSampleType, TextureUsages, TextureViewDimension,
};

pub struct AtlasRepository {
    texture_size: UVec3,
    atlas_0: HeaplessVec<Texture, MAX_ATLAS>,
    atlas_1: HeaplessVec<Texture, MAX_ATLAS>,
}

impl AtlasRepository {
    pub fn new(texture_size: UVec3) -> Self {
        Self {
            texture_size,
            atlas_0: HeaplessVec::new(),
            atlas_1: HeaplessVec::new(),
        }
    }

    pub fn add_atlas(&mut self, device: &Device, label: &'static str) -> Option<usize> {
        if self.atlas_0.len() < self.atlas_0.capacity() {
            let desc = TextureDescriptor {
                label: Some(label),
                dimension: TextureDimension::D3,
                size: Extent3d {
                    width: self.texture_size.x,
                    height: self.texture_size.y,
                    depth_or_array_layers: self.texture_size.z,
                },
                format: TextureFormat::Rgba32Float,
                mip_level_count: 1,
                sample_count: 1,
                usage: TextureUsages::COPY_SRC
                    | TextureUsages::COPY_DST
                    | TextureUsages::TEXTURE_BINDING
                    | TextureUsages::STORAGE_BINDING,
            };

            let tex0 = Texture::new(device, desc.clone());
            self.atlas_0.push(tex0).unwrap();
            let tex1 = Texture::new(device, desc);
            self.atlas_1.push(tex1).unwrap();
            Some(self.atlas_0.len() - 1)
        } else {
            None
        }
    }
}

const CHUNK_SIZE_WITH_APRONS: u32 = CHUNK_SIZE + 2;

pub struct AtlasCells {
    available_items: Vec<UVec3>,
}

impl AtlasCells {
    pub fn new(texture_size: UVec3) -> Self {
        let cells = texture_size / CHUNK_SIZE_WITH_APRONS;
        let mut items = Vec::new();
        let all_cells = (0..cells.x)
            .into_iter()
            .flat_map(|x| {
                (0..cells.y).into_iter().flat_map(move |y| {
                    (0..cells.z)
                        .into_iter()
                        .map(move |z| UVec3::new(x, y, z) * CHUNK_SIZE_WITH_APRONS + UVec3::ONE)
                })
            })
            .rev();
        items.extend(all_cells);
        Self {
            available_items: items,
        }
    }

    pub fn allocate_cell(&mut self) -> Option<UVec3> {
        self.available_items.pop()
    }

    pub fn reclaim_cell(&mut self, cell: UVec3) {
        self.available_items.push(cell)
    }
}

struct ChunksData {
    library_buffer: CPUOwnedData<ChunkLibrary>,
    aux_buffer: GPUOwnedData<ChunkAuxData>,
    atlases: AtlasRepository,
    atlas_cells: AtlasCells,
}

pub struct Chunks {
    data: ChunksData,
    pub bind_layout: BindGroupLayout,
    bind_group_0: BindGroup,
    bind_group_1: BindGroup,
    ordering_buffer: CPUOwnedData<Ordering>,
    pub ordering_bind_layout: BindGroupLayout,
    pub ordering_bind_group: BindGroup,
    swapped: bool,
}

impl Chunks {
    pub fn new(texture_size: UVec3, device: &Device) -> Self {
        let library = ChunkLibrary::default();
        let library_buffer = CPUOwnedData::new(library, &device, "chunk library");
        let aux_buffer = GPUOwnedData::new(&device, "chunk aux data");
        let atlas_cells = AtlasCells::new(texture_size);
        let mut atlases = AtlasRepository::new(texture_size);
        atlases.add_atlas(device, "material data");
        let data = ChunksData {
            library_buffer,
            aux_buffer,
            atlas_cells,
            atlases,
        };

        let bind_layout = Chunks::create_common_bind_layout(&data, device);
        let bind_group_0 = Chunks::create_bind_group(&bind_layout, &data, device, false);
        let bind_group_1 = Chunks::create_bind_group(&bind_layout, &data, device, true);

        let ordering_buffer = CPUOwnedData::new(Ordering::zeroed(), device, "ordering");
        let ordering_bind_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some("ordering bind group"),
            entries: &[BindGroupLayoutEntry {
                binding: 0,
                visibility: ShaderStages::COMPUTE,
                ty: ordering_buffer.binding_descriptor(),
                count: None,
            }],
        });
        let ordering_bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: Some("ordering bind group"),
            layout: &ordering_bind_layout,
            entries: &[BindGroupEntry {
                binding: 0,
                resource: ordering_buffer.binding(),
            }],
        });
        Self {
            data,
            bind_layout,
            bind_group_0,
            bind_group_1,
            ordering_buffer,
            ordering_bind_layout,
            ordering_bind_group,
            swapped: false,
        }
    }

    pub fn add_chunks<'a>(&'a mut self, initializer: &'a ComputePipeline) -> AddChunks<'a> {
        AddChunks::new(self, initializer)
    }

    fn library(&self) -> &ChunkLibrary {
        self.data.library_buffer.as_ref()
    }

    fn add_chunk(&mut self, cell: IVec3) -> Option<u32> {
        let mut x: ChunkFound = Default::default();
        if self.library().find_chunk(cell, &mut x) {
            return Some(x.id);
        }
        if (self.library().num_chunks as usize) < MAX_CHUNKS {
            let mut library = self.data.library_buffer.lock();
            let mut ordering = self.ordering_buffer.lock();
            let n = library.num_chunks as usize;
            let atlas_cell = self.data.atlas_cells.allocate_cell()?;
            library.world_cells[n] = cell.extend(0);
            library.atlas_origins[n] = atlas_cell.extend(0);
            library.num_chunks += 1;
            library.rehash();
            let last_idx = ordering.length as usize;
            ordering.length += 1;
            ordering.indices[last_idx] = n as u32;
            return Some(n as u32);
        }
        None
    }

    pub fn get_group(&self) -> &BindGroup {
        if self.swapped {
            &self.bind_group_1
        } else {
            &self.bind_group_0
        }
    }

    fn create_common_bind_layout(data: &ChunksData, device: &Device) -> BindGroupLayout {
        let num_atlases = data.atlases.atlas_0.len();
        let bind0 = BindGroupLayoutDescriptor {
            label: Some("Chunk data"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::VERTEX_FRAGMENT | ShaderStages::COMPUTE,
                    ty: data.library_buffer.binding_descriptor(),
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                    ty: data.aux_buffer.binding_descriptor(),
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 2,
                    visibility: ShaderStages::VERTEX_FRAGMENT | ShaderStages::COMPUTE,
                    ty: BindingType::Texture {
                        view_dimension: TextureViewDimension::D3,
                        sample_type: TextureSampleType::Float { filterable: true },
                        multisampled: false,
                    },
                    count: NonZeroU32::new(num_atlases as u32),
                },
                BindGroupLayoutEntry {
                    binding: 3,
                    visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                    ty: BindingType::StorageTexture {
                        view_dimension: TextureViewDimension::D3,
                        format: TextureFormat::Rgba32Float,
                        access: StorageTextureAccess::ReadWrite,
                    },
                    count: NonZeroU32::new(num_atlases as u32),
                },
            ],
        };
        device.create_bind_group_layout(&bind0)
    }

    fn create_bind_group(
        layout: &wgpu::BindGroupLayout,
        data: &ChunksData,
        device: &Device,
        swapped: bool,
    ) -> BindGroup {
        let ref atlases = data.atlases;
        let mut atlas_fw = &atlases.atlas_0;
        let mut atlas_bk = &atlases.atlas_1;

        if swapped {
            core::mem::swap(&mut atlas_fw, &mut atlas_bk);
        }

        let views_fw: HeaplessVec<_, 10> = atlas_fw.iter().map(|v| v.as_ref()).collect();
        let views_bk: HeaplessVec<_, 10> = atlas_bk.iter().map(|v| v.as_ref()).collect();

        let desc = BindGroupDescriptor {
            label: None,
            layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: data.library_buffer.binding(),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: data.aux_buffer.binding(),
                },
                BindGroupEntry {
                    binding: 2,
                    resource: BindingResource::TextureViewArray(&views_fw[..]),
                },
                BindGroupEntry {
                    binding: 3,
                    resource: BindingResource::TextureViewArray(&views_bk[..]),
                },
            ],
        };

        device.create_bind_group(&desc)
    }

    pub fn num_chunks(&self) -> u32 {
        self.data.library_buffer.as_ref().num_chunks
    }

    pub fn cuboid_draw_range(&self) -> Range<u32> {
        0..(self.num_chunks() * 36)
    }

    pub(crate) fn apply_ordering(
        &mut self,
        device: &Device,
        encoder: &mut CommandEncoder,
        pipeline: &ComputePipeline,
    ) {
        {
            let ordering = self.ordering_buffer.as_ref();
            let mut library = self.data.library_buffer.lock();
            ordering.apply(&mut library.world_cells);
            ordering.apply(&mut library.atlas_origins);
            library.num_chunks = ordering.length;
            library.rehash();
        }
        self.data.library_buffer.refresh_ordered(device, encoder);
        self.ordering_buffer.refresh_ordered(device, encoder);
        {
            let mut pass = encoder.begin_compute_pass(&ComputePassDescriptor {
                label: Some("update ordering"),
            });
            pass.set_pipeline(pipeline);
            pass.set_bind_group(0, self.get_group(), &[]);
            pass.set_bind_group(1, &self.ordering_bind_group, &[]);
            pass.dispatch(1, 1, 1);
        }
        {
            let mut ordering = self.ordering_buffer.lock();
            for i in 0..ordering.length {
                ordering.indices[i as usize] = i;
            }
        }
    }

    pub fn retain_chunks(&mut self, mut filter: impl FnMut(IVec3) -> bool) {
        {
            let mut ordering = self.ordering_buffer.lock();
            let library = self.data.library_buffer.as_ref();
            ordering.length = library.num_chunks;
            for i in (0..ordering.length).rev() {
                if !filter(library.world_cells[i as usize].truncate()) {
                    ordering.indices[i as usize] = ordering.indices[(ordering.length - 1) as usize];
                    ordering.length -= 1;
                    self.data
                        .atlas_cells
                        .reclaim_cell(library.atlas_origins[i as usize].truncate())
                }
            }
        }
    }

    pub fn sort_chunks(&mut self, cameradata: &vox_populi::RayTracingGlobalData) {
        let mut ordering = self.ordering_buffer.lock();
        let length = ordering.length as usize;
        let s = &mut ordering.indices[..length];
        let library = self.data.library_buffer.as_ref();
        s.sort_by(|&i, &j| {
            let i = i as usize;
            let j = j as usize;
            let zi = cameradata
                .world_to_projection
                .project_point3(library.world_cells[i].truncate().as_f32() * CHUNK_SIZE as f32)
                .z;
            let zj = cameradata
                .world_to_projection
                .project_point3(library.world_cells[j].truncate().as_f32() * CHUNK_SIZE as f32)
                .z;
            zi.partial_cmp(&zj).unwrap()
        });
    }

    pub fn swap(&mut self) {
        self.swapped = !self.swapped;
    }
}

pub struct AddChunks<'a> {
    chunks: &'a mut Chunks,
    generate: &'a ComputePipeline,
    range: u32,
}

impl<'a> AddChunks<'a> {
    fn new(chunks: &'a mut Chunks, generate: &'a ComputePipeline) -> Self {
        let range = chunks.num_chunks();
        Self {
            chunks,
            generate,
            range,
        }
    }

    pub fn add_chunk(&mut self, cell: IVec3) -> Option<u32> {
        self.chunks.add_chunk(cell)
    }

    pub fn execute(self, device: &Device, queue: &Queue) -> bool {
        let range = self.range;
        if range == self.chunks.num_chunks() {
            return false;
        }
        let mut encoder = device.create_command_encoder(&CommandEncoderDescriptor {
            label: Some("generate chunks"),
        });
        let bind_group = self.chunks.get_group();
        if range > 0 {
            let mut min = UVec3::splat(u32::MAX);
            let mut max = UVec3::splat(0);
            for i in 0..range {
                let atlas_origin = self.chunks.library().atlas_origins[i as usize].truncate();
                let chunkmin = atlas_origin - UVec3::ONE;
                let chunkmax = atlas_origin + UVec3::splat(CHUNK_SIZE) + UVec3::ONE;
                min = min.min(chunkmin);
                max = max.max(chunkmax);
            }
            let origin = Origin3d {
                x: min.x,
                y: min.y,
                z: min.z,
            };
            let extent = max - min;
            let extent = Extent3d {
                width: extent.x,
                height: extent.y,
                depth_or_array_layers: extent.z,
            };

            let ref atlases = self.chunks.data.atlases;

            let mut atlas_fw = &atlases.atlas_0;
            let mut atlas_bk = &atlases.atlas_1;
            if self.chunks.swapped {
                core::mem::swap(&mut atlas_fw, &mut atlas_bk);
            }

            for (ai, ao) in atlas_fw.iter().zip(atlas_bk.iter()) {
                let source = ImageCopyTexture {
                    origin,
                    aspect: TextureAspect::All,
                    mip_level: 0,
                    texture: &ai.texture,
                };
                let target = ImageCopyTexture {
                    origin,
                    aspect: TextureAspect::All,
                    mip_level: 0,
                    texture: &ao.texture,
                };

                encoder.copy_texture_to_texture(source, target, extent);
            }
        }

        self.chunks
            .data
            .library_buffer
            .refresh_ordered(&device, &mut encoder);

        let desc = ComputePassDescriptor {
            label: Some("generate"),
        };
        {
            let mut pass = encoder.begin_compute_pass(&desc);
            pass.set_pipeline(&self.generate);
            pass.set_bind_group(0, &bind_group, &[]);
            let range = range..self.chunks.num_chunks();
            pass.set_push_constants(0, bytemuck::bytes_of(&[range.start, range.end]));
            pass.dispatch(range.end - range.start, 1, CHUNK_SIZE);
        }

        queue.submit(Some(encoder.finish()));
        true
    }
}
