use std::ops::Range;
use wgpu::{
    BindGroup, BindGroupLayout, BindingResource, BindingType, Buffer, BufferAddress, BufferSize,
    ComputePipeline, Device, PipelineLayout, Queue, Texture,
};

#[derive(Eq, PartialEq)]
enum Mutability {
    Immutable,
    Mutable,
}

enum StaticBindingResource<'a> {
    Buffer(&'a Buffer, Range<BufferAddress>, Mutability),
}

struct ComputeFunction<T: bytemuck::Pod> {
    group_layouts: Vec<BindGroupLayout>,
    groups: Vec<BindGroup>,
    pipeline_layout: PipelineLayout,
    pipeline: ComputePipeline,
    label: Option<String>,
}

impl<'a> StaticBindingResource<'a> {
    fn binding_type(&self) -> BindingType {
        match *self {
            Self::Buffer(_, ref range, ref mutability) => BindingType::Buffer {
                ty: wgpu::BufferBindingType::Storage {
                    read_only: mutability == Mutability::Immutable,
                },
                min_binding_size: Some((range.end - range.start) as BufferSize),
                has_dynamic_offset: false,
            },
        }
    }

    fn binding_resource(&self) -> BindingResource<'a> {
        match *self {
            Self::Buffer(buffer, ref range, ref mutability) => {
                BindingResource::Buffer(wgpu::BufferBinding {
                    buffer,
                    offset: range.start,
                    size: Some((range.end - range.start) as BufferSize),
                })
            }
        }
    }
}

impl<T: bytemuck::Pod> ComputeFunction<T> {
    pub fn new(
        label: Option<impl ToString>,
        device: &Device,
        bindings: &[(u32, u32, StaticBindingResource)],
    ) -> Self {
        use wgpu::ShaderStage;

        let label = label.map(|x| x.to_string());

        let num_bind_groups = bindings
            .iter()
            .map(|(set, _, _)| set + 1)
            .max()
            .unwrap_or(0);

        let mut group_layouts = Vec::new();
        let mut groups = Vec::new();

        group_layouts.reserve(num_bind_groups as usize);
        groups.reserve(num_bind_groups as usize);

        for bind_group_id in num_bind_groups {
            use wgpu::{
                BindGroupDescriptor, BindGroupEntry, BindGroupLayoutDescriptor,
                BindGroupLayoutEntry,
            };
            let bindings = bindings.iter().filter_map(|(set, binding, resource)| {
                if set == bind_group_id {
                    Some((binding, resource))
                } else {
                    None
                }
            });
            {
                let layout_entries: Vec<_> = bindings
                    .map(|(&binding, resource)| BindGroupLayoutEntry {
                        binding,
                        visibility: ShaderStage::COMPUTE,
                        ty: resource.binding_type(),
                        count: None,
                    })
                    .collect();

                let desc = BindGroupLayoutDescriptor {
                    label: None,
                    entries: &layout_entries,
                };

                group_layouts.push(device.create_bind_group_layout(&desc));
            }

            {
                let group_entries: Vec<_> = bindings
                    .map(|(&binding, resource)| BindGroupEntry {
                        binding,
                        resource: resource.binding_resource(),
                    })
                    .collect();
                let desc = BindGroupDescriptor {
                    label: None,
                    layout: group_layouts.last().unwrap(),
                    entries: &group_entries,
                };

                groups.push(device.create_bind_group(&desc));
            }
        }

        let pipeline_layout;
        let pipeline;

        {
            use wgpu::{PipelineLayoutDescriptor, PushConstantRange};
            let desc = PipelineLayoutDescriptor {
                label: None,
                bind_group_layouts: &group_layouts.iter().collect::<Vec<_>>(),
                push_constant_ranges: &[PushConstantRange {
                    stages: ShaderStage::COMPUTE,
                    range: 0..(std::mem::size_of::<T>() as u32),
                }],
            };

            pipeline_layout = device.create_pipeline_layout(&desc);
        }

        {
            use wgpu::ComputePipelineDescriptor;

            let desc = ComputePipelineDescriptor {
                label: None,
                layout: Some(&pipeline_layout),
                module: todo!(),
                entry_point: todo!(),
            };

            pipeline = device.create_compute_pipeline(&desc);
        }

        Self {
            group_layouts,
            groups,
            pipeline_layout,
            pipeline,
            label,
        }
    }
}
