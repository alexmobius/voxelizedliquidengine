#![windows_subsystem = "console"]

extern crate bytemuck;
extern crate futures;
extern crate heapless;
extern crate vox_populi;
extern crate wgpu;
extern crate winit;

mod camera;
//mod compute;
mod buffer;
mod shaders;
mod targets;
mod texture;
mod voxel_data;

use glam::uvec3;
use std::path::PathBuf;
use std::str::FromStr;
use std::time::Instant;
use wgpu::{CommandEncoderDescriptor, ShaderStages};
use winit::dpi::PhysicalSize;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

fn main() {
    use futures::executor::block_on;
    env_logger::init();
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_inner_size(PhysicalSize {
            width: 800u32,
            height: 600u32,
        })
        .with_title("MY Nice App")
        .build(&event_loop)
        .unwrap();

    let size = window.inner_size();
    let instance = wgpu::Instance::new(wgpu::Backends::VULKAN);

    let surface = unsafe { instance.create_surface(&window) };

    let adapter = block_on(instance.request_adapter(&wgpu::RequestAdapterOptions {
        power_preference: wgpu::PowerPreference::HighPerformance,
        force_fallback_adapter: false,
        compatible_surface: Some(&surface),
    }))
    .unwrap();

    let (device, queue) = block_on(adapter.request_device(
        &wgpu::DeviceDescriptor {
            label: None,
            features: wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
                | wgpu::Features::TEXTURE_BINDING_ARRAY
                | wgpu::Features::PUSH_CONSTANTS
                | wgpu::Features::SPIRV_SHADER_PASSTHROUGH
                | wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
                | wgpu::Features::STORAGE_RESOURCE_BINDING_ARRAY
                | wgpu::Features::CONSERVATIVE_RASTERIZATION
                | wgpu::Features::DEPTH_CLIP_CONTROL
                | wgpu::Features::UNSIZED_BINDING_ARRAY,
            limits: wgpu::Limits {
                max_push_constant_size: std::mem::size_of::<vox_populi::RayTracingGlobalData>()
                    as u32,
                max_bind_groups: 3,
                ..Default::default()
            },
        },
        Some(&PathBuf::from_str("./traces").unwrap()),
    ))
    .unwrap();

    let mut sc_desc = wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        format: surface.get_preferred_format(&adapter).unwrap(),
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Fifo,
    };
    surface.configure(&device, &sc_desc);
    println!("Swapchain created!");
    let mut cam_desc = camera::CameraSettings {
        screen_size: size,
        unit_size: 20.,
        distance_to_camera: 128.,
        distance_to_far_plane: 128.,
        perspective: 0.,
    };
    let mut camera = camera::Camera::new(glam::vec3(8.0, 8.0, 16.0), &cam_desc);

    let mut chunks = voxel_data::Chunks::new(uvec3(512, 512, 18), &device);
    let mut render_target = targets::Targets::new(size, &device);

    let debug = targets::DebugShow::Offset;
    let shader = shaders::Shaders::new(
        &device,
        &chunks,
        &render_target.color_target_states(debug, sc_desc.format),
    );

    let mut moment = Instant::now();

    println!("Let's gooo!");
    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == window.id() => match event {
            WindowEvent::Resized(new_size) => {
                sc_desc.width = new_size.width;
                sc_desc.height = new_size.height;
                surface.configure(&device, &sc_desc);
                cam_desc.screen_size = *new_size;
                camera.set_settings(&cam_desc);
                render_target = targets::Targets::new(*new_size, &device);
            }
            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
            WindowEvent::KeyboardInput { input, .. } => match input {
                KeyboardInput {
                    state: ElementState::Pressed,
                    virtual_keycode: Some(keycode),
                    ..
                } => {
                    use VirtualKeyCode::*;
                    match keycode {
                        Escape => *control_flow = ControlFlow::Exit,
                        W => {
                            let fwd = camera.forward();
                            camera.look_at += glam::vec3(fwd.x, fwd.y, 0.0);
                            window.request_redraw();
                        }
                        S => {
                            let fwd = camera.forward();
                            camera.look_at += glam::vec3(-fwd.x, -fwd.y, 0.0);
                            window.request_redraw();
                        }
                        A => {
                            camera.rotate_ccw();
                        }
                        D => {
                            camera.rotate_cw();
                        }
                        Space => {}
                        _ => {}
                    }
                }
                _ => {}
            },
            _ => {}
        },
        Event::MainEventsCleared => {
            if !camera.is_stable() {
                window.request_redraw()
            }
            let now = Instant::now();
            camera.time_passes((now - moment).as_secs_f32() / 2.0);
            moment = now;
        }
        Event::RedrawRequested(window_id) if window.id() == window_id => {
            println!("redraw");
            let (min, max, test) = camera.viewable_chunks();
            chunks.retain_chunks(&test);

            {
                let mut adder = chunks.add_chunks(&shader.generate_pipeline);
                for x in min.x..=max.x {
                    for y in min.y..=max.y {
                        for z in 0..=max.z {
                            let chunk = glam::IVec3::new(x, y, z);
                            if test(chunk) {
                                adder.add_chunk(chunk).expect(&format!(
                                    "chunk {}, {}, {} failed, camera state {:?}",
                                    x, y, z, camera
                                ));
                            }
                        }
                    }
                }
                if adder.execute(&device, &queue) {
                    chunks.swap();
                }
            }

            let start = Instant::now();
            let gdata = camera.get_global_data();
            chunks.sort_chunks(&gdata);

            let draw_range = chunks.cuboid_draw_range();

            let target = surface.get_current_texture().unwrap();
            let view = target.texture.create_view(&Default::default());

            let mut encoder = device.create_command_encoder(&CommandEncoderDescriptor {
                label: Some("main render"),
            });
            chunks.apply_ordering(&device, &mut encoder, &shader.apply_ordering_pipeline);
            println!("refreshed");
            let bytes_of_gdata = bytemuck::bytes_of(&gdata);
            let group = chunks.get_group();
            {
                let mut pass = render_target.begin_render_pass(&mut encoder, debug, &view);
                pass.set_pipeline(&shader.render_pipeline);
                pass.set_bind_group(0, &group, &[]);
                pass.set_push_constants(ShaderStages::VERTEX_FRAGMENT, 0, &bytes_of_gdata);
                pass.set_stencil_reference(0xff);
                pass.draw(draw_range, 0..1);
            }
            queue.submit(std::iter::once(encoder.finish()));
            target.present();
            println!(
                "Frame time: {}ms, {} chunks",
                (Instant::now() - start).as_millis(),
                chunks.num_chunks()
            )
        }
        _ => {}
    });
}
