use crate::any;
/// Algorithm due to A Fast Voxel Traversal Algorithm for Ray Tracing
/// John Amanatides
/// Andrew Woo
use crate::glam::{BVec3, IVec3, UVec3, Vec3, Vec3Swizzles};

pub struct RayHit {
    pub feature: BVec3,
    pub exit_time: f32,
    _filler: u32,
}

#[derive(Clone)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
    pub direction_signum: IVec3,
}

pub struct DDA {
    delta_coord: Vec3,
    delta_quant: IVec3,
    next_coord_hit: Vec3,
    pub t: f32,
    pub quantized_tip: IVec3,
    pub feature: BVec3,
}

#[cfg(target_arch = "spirv")]
fn isign(v: Vec3) -> IVec3 {
    let mut res = IVec3::ZERO;
    unsafe {
        asm! {
        "%glsl = OpExtInstImport \"GLSL.std.450\"",
        "%v = OpLoad typeof*{v} {v}",
        "%sign = OpExtInst typeof*{v} %glsl 6 %v",
        "%s = OpConvertFToS typeof*{res} %sign",
        "OpStore {res} %s",
        v = in(reg) &v,
        res = in(reg) &mut res
        }
    }
    res
}
#[cfg(not(target_arch = "spirv"))]
fn isign(v: Vec3) -> IVec3 {
    v.signum().as_i32()
}

impl Ray {
    pub fn new(origin: Vec3, direction: Vec3) -> Self {
        Self {
            origin,
            direction,
            direction_signum: isign(direction),
        }
    }

    pub fn dda(&self, delta: UVec3, offset: IVec3, start_t: f32) -> DDA {
        let deltaf = delta.as_f32();
        let cur_point = (self.origin + self.direction * start_t) / deltaf;
        let delta_coord = (deltaf / self.direction).abs();
        let signum_as_f32 = Vec3::from(self.direction_signum.as_f32());
        let delta_to_corner =
            (cur_point.floor() - cur_point + Vec3::splat(0.5)) * signum_as_f32 + Vec3::splat(0.5);
        let backwards_delta_to_opposite_corner = Vec3::ONE - delta_to_corner;
        let previous_hit = backwards_delta_to_opposite_corner * delta_coord;
        let mut initial_mask =
            previous_hit.cmple(previous_hit.yzx()) & previous_hit.cmplt(previous_hit.zxy());
        if !any(initial_mask) {
            initial_mask = BVec3::new(true, false, false);
        }
        let next_coord_hit = delta_to_corner * delta_coord + Vec3::splat(start_t);
        DDA {
            delta_quant: self.direction_signum,
            delta_coord,
            next_coord_hit,
            t: start_t,
            quantized_tip: cur_point.floor().as_i32() - offset,
            feature: initial_mask,
        }
    }

    #[inline(always)]
    pub fn at(&self, t: f32) -> Vec3 {
        self.origin + self.direction * t
    }
}

impl DDA {
    #[inline(always)]
    pub fn next(&self) -> RayHit {
        let mut mask = self.next_coord_hit.cmplt(self.next_coord_hit.yzx())
            & self.next_coord_hit.cmple(self.next_coord_hit.zxy());
        let [mx, my, mz]: [bool; 3] = mask.into();
        let time = if mx {
            self.next_coord_hit.x
        } else if my {
            self.next_coord_hit.y
        } else if mz {
            self.next_coord_hit.z
        } else {
            mask = BVec3::new(true, false, false);
            self.next_coord_hit.x
        };

        RayHit {
            feature: mask,
            exit_time: time,
            _filler: 0,
        }
    }
    #[inline(always)]
    pub fn advance(&mut self, hit: RayHit) {
        let RayHit {
            feature,
            exit_time: time,
            ..
        } = hit;
        self.feature = feature;
        self.t = time;
        self.next_coord_hit += Vec3::select(feature, self.delta_coord, Vec3::ZERO);
        self.quantized_tip += IVec3::select(feature, self.delta_quant, IVec3::ZERO);
    }
}
