#![no_std]
#![deny(warnings)]
#![allow(incomplete_features)]
#![feature(asm, asm_experimental_arch, adt_const_params, generic_const_exprs)]
#![deny(warnings)]

pub use bytemuck;

pub mod chunks;
pub mod dda;
pub mod finite_difference;
pub mod particle;
pub mod raytracing;
pub mod spacecurve;

#[cfg(target_arch = "spirv")]
pub use spirv_std::glam;
#[cfg(not(target_arch = "spirv"))]
pub extern crate glam;

pub use chunks::ChunkLibrary;
pub use raytracing::RayTracingGlobalData;

#[cfg(target_arch = "spirv")]
pub use spirv_std::arch::any;
#[cfg(not(target_arch = "spirv"))]
pub fn any(x: glam::BVec3) -> bool {
    x.any()
}

#[cfg(target_arch = "spirv")]
pub use spirv_std::arch::all;
#[cfg(not(target_arch = "spirv"))]
pub fn all(x: glam::BVec3) -> bool {
    x.all()
}
