const X_SHADOW: u32 = 0b001_001_001;
const Y_SHADOW: u32 = 0b011_110_101;
const Z_SHADOW: u32 = 0b111_100_010;

const LEVEL_2: u32 = 0b1_000_000_000;

macro_rules! head_shadow {
    ($firstshade:ident, $secondshade:ident, $thirdshade:ident) => {
        shadow_list!(@marker
            ($firstshade * (LEVEL_2 + 1)),
            ($secondshade * (LEVEL_2 + 1)) >> 3,
            ($thirdshade * (LEVEL_2 + 1)) >> 6,
            $firstshade,
            $secondshade >> 3,
            $thirdshade >> 6
        )
    };
}

const fn nbrl3(a: u32) -> u32 {
    a >> 3 ^ (a & 0b111) << 6
}

const fn nbrl6(a: u32) -> u32 {
    a >> 6 ^ (a & 0b111) << 3
}

macro_rules! long_shadow {
    ($firstshade:ident, $secondshade:ident, $thirdshade:ident) => {
        shadow_list!(
            @marker
            $firstshade * (LEVEL_2 + 1),
            nbrl3($secondshade) * (LEVEL_2 + 1),
            nbrl6($thirdshade) * (LEVEL_2 + 1),
            $firstshade * (LEVEL_2 + 1),
            nbrl3($secondshade) * (LEVEL_2 + 1),
            nbrl6($thirdshade) * (LEVEL_2 + 1)
        )
    };
}

macro_rules! shadow_list {
    ($(@marker $next:expr, $($suffix:expr),*);+) => {
        shadow_list!($(
            0, @marker $($suffix),*;
            $next, @marker $($suffix),*
        );+)
    };
    ($($($prefix:expr),*, @marker $next:expr, $($suffix:expr),*);+) => {
        shadow_list!($(
            $($prefix),*, 0, @marker $($suffix),*;
            $($prefix),*, $next, @marker $($suffix),*
        );+)
    };
    ($($($prefix:expr),*, @marker $next:expr);+) => {
        shadow_list!($(
            $($prefix),*, 0, @marker;
            $($prefix),*, $next, @marker
        );+)
    };
    ($($($prefix:expr),*, @marker);+) => {
        [
            $($(($prefix))^*),+
        ]

    };
}

const X_6BIT_HEAD_SHADOW: [u32; 64] = head_shadow!(X_SHADOW, Z_SHADOW, Y_SHADOW);
const Y_6BIT_HEAD_SHADOW: [u32; 64] = head_shadow!(Y_SHADOW, X_SHADOW, Z_SHADOW);
const Z_6BIT_HEAD_SHADOW: [u32; 64] = head_shadow!(Z_SHADOW, Y_SHADOW, X_SHADOW);

const X_6BIT_LONG_SHADOW: [u32; 64] = long_shadow!(X_SHADOW, Z_SHADOW, Y_SHADOW);
const Y_6BIT_LONG_SHADOW: [u32; 64] = long_shadow!(Y_SHADOW, X_SHADOW, Z_SHADOW);
const Z_6BIT_LONG_SHADOW: [u32; 64] = long_shadow!(Z_SHADOW, Y_SHADOW, X_SHADOW);

extern crate unroll;
use unroll::unroll_for_loops;

#[allow(unused_assignments)]
#[unroll_for_loops]
pub fn peano_order(v: crate::glam::IVec3) -> u64 {
    let v = v.as_u32();
    const BITMASK_6: u32 = 0b111_111;
    let mut shadow = 0;
    let mut result: u64 = 0;
    for i in 1..=5 {
        let shift = 30 - i * 6;
        result = (result << 18) ^ shadow;
        result ^= X_6BIT_HEAD_SHADOW[((v.x >> shift) & BITMASK_6) as usize] as u64;
        result ^= Y_6BIT_HEAD_SHADOW[((v.y >> shift) & BITMASK_6) as usize] as u64;
        result ^= Z_6BIT_HEAD_SHADOW[((v.z >> shift) & BITMASK_6) as usize] as u64;
        shadow ^= X_6BIT_LONG_SHADOW[((v.x >> shift) & BITMASK_6) as usize] as u64;
        shadow ^= Y_6BIT_LONG_SHADOW[((v.y >> shift) & BITMASK_6) as usize] as u64;
        shadow ^= Z_6BIT_LONG_SHADOW[((v.z >> shift) & BITMASK_6) as usize] as u64;
    }

    result
}

#[cfg(test)]
extern crate std;

#[test]
fn print_some_stuff() {
    use glam::IVec3;
    println!("{:018b}", &X_6BIT_HEAD_SHADOW[0b111000]);
    println!("{:018b}", &X_6BIT_LONG_SHADOW[0b111]);
    for po2 in 1..13 {
        let val = (1 << po2) - 1;
        println!(
            "x, {}: {:09b}",
            po2,
            peano_order(IVec3::new(val, 0, 0)) & (LEVEL_2 - 1) as u64
        );
        println!(
            "y, {}: {:09b}",
            po2,
            peano_order(IVec3::new(0, val, 0)) & (LEVEL_2 - 1) as u64
        );
        println!(
            "z, {}: {:09b}",
            po2,
            peano_order(IVec3::new(0, 0, val)) & (LEVEL_2 - 1) as u64
        );
    }
}
