use crate::glam::{Vec3, Vec4};

#[repr(u32)]
#[derive(Copy, Clone)]
pub enum State {
    Static,
    Solid,
    Liquid,
    Gas,
}

#[repr(C, align(16))]
#[derive(Clone)]
pub struct Particle {
    pub position_and_energy: Vec4,
    pub impulse_and_mass: Vec4,
    pub state: State,
    pub material_index: u32,
    pub cell_offset: u32,
}

impl Particle {
    #[inline]
    pub fn position(&self) -> Vec3 {
        self.position_and_energy.truncate()
    }

    #[inline]
    pub fn energy(&self) -> f32 {
        self.position_and_energy.w
    }

    #[inline]
    pub fn impulse(&self) -> Vec3 {
        self.impulse_and_mass.truncate()
    }

    #[inline]
    pub fn mass(&self) -> f32 {
        self.impulse_and_mass.w
    }
}
