use core::marker::PhantomData;
use core::ops::Sub;

pub enum StorageKind {
    Center,
    Staggered,
}

pub struct Center<const OFFSET: i32>();
pub struct Staggered<const OFFSET: i32>();

trait GridOffset: 'static + Sized {
    unsafe fn offset() -> i32;
    unsafe fn offset_f32() -> f32;
    const KIND: StorageKind;
}

impl<const OFFSET: i32> GridOffset for Center<OFFSET> {
    unsafe fn offset() -> i32 {
        OFFSET
    }
    unsafe fn offset_f32() -> f32 {
        OFFSET as f32
    }
    const KIND: StorageKind = StorageKind::Center;
}

impl<const OFFSET: i32> GridOffset for Staggered<OFFSET> {
    unsafe fn offset() -> i32 {
        OFFSET
    }
    unsafe fn offset_f32() -> f32 {
        OFFSET as f32 + 0.5
    }
    const KIND: StorageKind = StorageKind::Staggered;
}

trait Nearest: GridOffset {
    type PlusHalf: GridOffset;
    type MinusHalf: GridOffset;
}

const fn as_usize(x: i32) -> usize {
    x as usize
}

impl<const OFFSET: i32> Nearest for Center<OFFSET>
where
    [(); as_usize(OFFSET)]: Sized,
    Staggered<{ OFFSET - 1 }>: 'static,
{
    type PlusHalf = Staggered<OFFSET>;
    type MinusHalf = Staggered<{ OFFSET - 1 }>;
}

impl<const OFFSET: i32> Nearest for Staggered<OFFSET>
where
    [(); as_usize(-OFFSET)]: Sized,
    Center<{ OFFSET + 1 }>: 'static,
{
    type PlusHalf = Center<{ OFFSET + 1 }>;
    type MinusHalf = Center<OFFSET>;
}

trait WithinHalf<T>: 'static + Sized {}

impl<const OFFSET: i32> WithinHalf<Center<OFFSET>> for Staggered<OFFSET> {}
impl<const OFFSET: i32> WithinHalf<Center<{ OFFSET + 1 }>> for Staggered<OFFSET> where
    Center<{ OFFSET + 1 }>: 'static
{
}
impl<const OFFSET: i32> WithinHalf<Staggered<OFFSET>> for Center<OFFSET> {}
impl<const OFFSET: i32> WithinHalf<Staggered<{ OFFSET - 1 }>> for Center<OFFSET> where
    Staggered<{ OFFSET - 1 }>: 'static
{
}
impl<T: GridOffset> WithinHalf<T> for T {}

#[repr(transparent)]
#[allow(unused)]
struct VoxelValue<V, OffsetX: GridOffset, OffsetY: GridOffset, OffsetZ: GridOffset>(
    pub V,
    PhantomData<(OffsetX, OffsetY, OffsetZ)>,
);

impl<V, OffsetX: GridOffset, OffsetY: GridOffset, OffsetZ: GridOffset>
    VoxelValue<V, OffsetX, OffsetY, OffsetZ>
where
    V: Sub<V, Output = V>,
    OffsetX: Nearest,
{
    #[allow(unused)]
    pub fn diff_x(
        iminus: VoxelValue<V, OffsetX::MinusHalf, OffsetY, OffsetZ>,
        iplus: VoxelValue<V, OffsetX::PlusHalf, OffsetY, OffsetZ>,
    ) -> Self {
        Self(iplus.0 - iminus.0, PhantomData)
    }
}

impl<V, OffsetX: GridOffset, OffsetY: GridOffset, OffsetZ: GridOffset>
    VoxelValue<V, OffsetX, OffsetY, OffsetZ>
where
    V: Sub<V, Output = V>,
    OffsetY: Nearest,
{
    #[allow(unused)]
    pub fn diff_y(
        iminus: VoxelValue<V, OffsetX, OffsetY::MinusHalf, OffsetZ>,
        iplus: VoxelValue<V, OffsetX, OffsetY::PlusHalf, OffsetZ>,
    ) -> Self {
        Self(iplus.0 - iminus.0, PhantomData)
    }
}

impl<V, OffsetX: GridOffset, OffsetY: GridOffset, OffsetZ: GridOffset>
    VoxelValue<V, OffsetX, OffsetY, OffsetZ>
where
    V: Sub<V, Output = V>,
    OffsetZ: Nearest,
{
    #[allow(unused)]
    pub fn diff_z(
        iminus: VoxelValue<V, OffsetX, OffsetY, OffsetZ::MinusHalf>,
        iplus: VoxelValue<V, OffsetX, OffsetY, OffsetZ::PlusHalf>,
    ) -> Self {
        Self(iplus.0 - iminus.0, PhantomData)
    }
}
