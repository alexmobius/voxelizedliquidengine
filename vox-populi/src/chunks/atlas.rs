use spirv_std::macros::Image;
use spirv_std::Sampler;

use super::*;
use crate::all;
use crate::glam::{IVec3, UVec3, Vec3, Vec4};

pub type AtlasIn = Image!(3D, format=rgba32f, sampled=true);
pub type AtlasOut = Image!(3D, format=rgba32f, sampled=false);

pub trait SimpleChunkAccess {
    fn fetch(&self, coord: IVec3, atlas: &AtlasIn) -> Vec4;
    fn trilinear_sample(&self, coord: Vec3, sampler: Sampler, atlas: &AtlasIn) -> Vec4;
    fn write(&self, coord: IVec3, atlas: &AtlasOut, value: Vec4);
}

impl SimpleChunkAccess for ChunkFound {
    #[inline(always)]
    fn fetch(&self, coord: IVec3, atlas: &AtlasIn) -> Vec4 {
        atlas.fetch(self.atlas_origin.as_i32() + coord)
    }

    #[inline(always)]
    fn trilinear_sample(&self, coord: Vec3, sampler: Sampler, atlas: &AtlasIn) -> Vec4 {
        let size = atlas.query_size_lod::<UVec3>(0).as_f32();
        let coord = self.atlas_origin.as_f32() + coord;
        atlas.sample_by_lod(sampler, coord / size, 0.0)
    }

    #[inline(always)]
    fn write(&self, coord: IVec3, atlas: &AtlasOut, value: Vec4) {
        assert!(all(coord.cmpge(-IVec3::ONE)) && all(coord.cmple(IVec3::splat(CHUNK_SIZE as i32))));
        unsafe {
            atlas.write(self.atlas_origin.as_i32() + coord, value);
        }
    }
}
