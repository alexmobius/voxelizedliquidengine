use crate::glam::{IVec3, IVec4, UVec3, UVec4};
use bytemuck::{Pod, Zeroable};

#[cfg(target_arch = "spirv")]
pub mod atlas;

pub const CHUNK_SIZE: u32 = 16;
pub const MAX_CHUNKS: usize = 256;
pub const HASH_SIZE: usize = MAX_CHUNKS * 2;
pub const HASH_LEN: u32 = 10;
pub const MAX_ATLAS: usize = 10;

use bitfield::bitfield;

bitfield! {
    #[derive(Clone, Copy, Default)]
    #[repr(transparent)]
    pub struct Flags(u32);
    pub visible, set_visible: 0;
    pub u8, border, set_border: 7, 2, 6;
}

unsafe impl Zeroable for Flags {}
unsafe impl Pod for Flags {}

pub enum StandardAtlas {
    MaterialData = 0,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct ChunkLibrary {
    pub world_cells: [IVec4; MAX_CHUNKS],
    pub atlas_origins: [UVec4; MAX_CHUNKS],
    pub hash_lookup: [u32; HASH_SIZE],
    pub num_chunks: u32,
    pub _padding: [u8; 12],
}

unsafe impl Zeroable for ChunkLibrary {}
unsafe impl Pod for ChunkLibrary {}

#[cfg(not(target_arch = "spirv"))]
impl Default for ChunkLibrary {
    fn default() -> Self {
        Self {
            world_cells: [IVec4::ZERO; MAX_CHUNKS],
            atlas_origins: [UVec4::ZERO; MAX_CHUNKS],
            hash_lookup: [u32::MAX; HASH_SIZE],
            num_chunks: 0,
            _padding: Zeroable::zeroed(),
        }
    }
}

#[derive(Default)]
pub struct ChunkFound {
    pub world_cell: IVec3,
    pub id: u32,
    pub atlas_origin: UVec3,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct ChunkAuxData {
    pub flags: [Flags; MAX_CHUNKS],
}

unsafe impl Zeroable for ChunkAuxData {}
unsafe impl Pod for ChunkAuxData {}

/// Ordering for chunk library/aux
#[repr(C)]
#[derive(Clone, Copy)]
pub struct Ordering {
    pub indices: [u32; MAX_CHUNKS],
    pub length: u32,
}

unsafe impl Zeroable for Ordering {}
unsafe impl Pod for Ordering {}

#[inline]
fn hash_cell(v: IVec3) -> u32 {
    const FACTOR: u32 = 1103515245;
    let x = v.x as u32;
    let y = v.y as u32;
    let z = v.z as u32;
    x.wrapping_mul(FACTOR)
        .wrapping_add(y)
        .wrapping_mul(FACTOR)
        .wrapping_add(z)
        .wrapping_mul(FACTOR)
        .wrapping_add(13)
        % (HASH_SIZE as u32)
}

impl ChunkLibrary {
    pub fn find_chunk_index(&self, world_cell: IVec3) -> Option<u32> {
        let hash = hash_cell(world_cell);

        for offset in 0..HASH_LEN {
            let j = (hash + offset) % (HASH_SIZE as u32);
            let i = self.hash_lookup[j as usize];
            if i == u32::MAX {
                return None;
            }
            if self.world_cells[i as usize].truncate() == world_cell {
                return Some(i);
            }
        }
        None
    }

    pub fn find_chunk(&self, world_cell: IVec3, chunk_found: &mut ChunkFound) -> bool {
        if let Some(i) = self.find_chunk_index(world_cell) {
            let i = i as usize;
            *chunk_found = ChunkFound {
                id: i as u32,
                atlas_origin: self.atlas_origins[i].truncate(),
                world_cell,
            };
            return true;
        }
        false
    }

    pub fn get_chunk(&self, idx: u32) -> ChunkFound {
        if idx < self.num_chunks {
            ChunkFound {
                id: idx,
                atlas_origin: self.atlas_origins[idx as usize].truncate(),
                world_cell: self.world_cells[idx as usize].truncate(),
            }
        } else {
            panic!()
        }
    }

    pub fn remove_chunk_by_index(&mut self, index: u32) {
        let idx = index as usize;
        let last = (self.num_chunks - 1) as usize;
        self.world_cells[idx] = self.world_cells[last];
        self.atlas_origins[idx] = self.atlas_origins[last];
        self.num_chunks -= 1;
        self.rehash();
    }

    pub fn rehash(&mut self) {
        for i in 0..HASH_SIZE {
            self.hash_lookup[i] = u32::MAX;
        }

        let hasher = |v: IVec4| hash_cell(v.truncate());

        let hash_size = HASH_SIZE as u32;

        let mut max_offset = 0;
        for mut i in 0..self.num_chunks {
            let mut h = hasher(self.world_cells[i as usize]);
            let mut offset = 0;
            while offset < HASH_LEN {
                let idx = (h + offset) % hash_size;
                match self.hash_lookup[idx as usize] {
                    u32::MAX => {
                        self.hash_lookup[idx as usize] = i;
                        break;
                    }
                    i_other => {
                        let hash_other = hasher(self.world_cells[i_other as usize]);
                        let offset_other = ((hash_other + hash_size) - idx) % hash_size;
                        if offset_other < offset {
                            self.hash_lookup[idx as usize] = i;
                            i = i_other;
                            h = hash_other;
                            offset = offset_other;
                        }
                    }
                }
                offset += 1;
            }
            if offset == HASH_LEN {
                panic!("hash chain too long")
            }
            max_offset = max_offset.max(offset);
        }
        for i in 0..self.num_chunks {
            let mut chunk = Default::default();
            assert!(self.find_chunk(self.world_cells[i as usize].truncate(), &mut chunk))
        }
    }
}

pub fn voxel_to_chunk(voxel: IVec3) -> IVec3 {
    IVec3::new(
        voxel.x.div_euclid(CHUNK_SIZE as i32),
        voxel.y.div_euclid(CHUNK_SIZE as i32),
        voxel.z.div_euclid(CHUNK_SIZE as i32),
    )
}

impl Ordering {
    pub fn apply<T: Default>(&self, data: &mut [T; MAX_CHUNKS])
    where
        [T; MAX_CHUNKS]: Copy,
    {
        let mut data_move_out = *data;

        for i in 0..(self.length as usize) {
            data[i] = core::mem::take(&mut data_move_out[self.indices[i] as usize]);
        }

        for v in &mut data[(self.length as usize)..MAX_CHUNKS] {
            *v = Default::default();
        }
    }
}
