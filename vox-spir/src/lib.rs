#![no_std]
#![cfg_attr(target_arch = "spirv", feature(register_attr), register_attr(spirv))]
#![allow(incomplete_features)]
#![feature(asm, asm_experimental_arch, adt_const_params, generic_const_exprs)]
//#![deny(warnings)]

#[cfg(not(target_arch = "spirv"))]
#[allow(unused_imports)]
use spirv_std::macros::spirv;
use spirv_std::RuntimeArray;
use vox_populi::chunks::{atlas::*, *};
use vox_populi::glam::{
    f32::{vec4, Vec3, Vec4},
    i32::{IVec3, IVec4},
    BVec3, Vec3Swizzles, Vec4Swizzles,
};
use vox_populi::raytracing;
use vox_populi::RayTracingGlobalData;

#[spirv(vertex)]
pub fn block_cuboid(
    #[spirv(vertex_index)] vert_id: u32,
    #[spirv(position)] out_pos: &mut Vec4,
    #[spirv(push_constant)] global_data: &RayTracingGlobalData,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] chunk_library: &ChunkLibrary,
    #[spirv(invariant)] out_world_pos: &mut Vec3,
    #[spirv(invariant, flat)] out_chunk_id: &mut u32,
) {
    let chunk_id = vert_id / 36;
    let face_id = (vert_id % 36) / 6;
    let triangle_id = (vert_id % 6) / 3;
    let vertex_id = vert_id % 3;

    let dir = face_id % 3;
    let pos = face_id / 3;

    let nz = dir >> 1;
    let ny = dir & 1;
    let nx = 1 ^ (nz | ny);

    let flip = 1 - 2 * (pos as i32);

    let d = IVec3::new(nx as i32, ny as i32, nz as i32);
    let n = d * flip;
    let u = -d.yzx();
    let v = d.zxy() * flip;

    let mirror = 2 * (triangle_id as i32) - 1;

    let xyz = n
        + u * (mirror * (1 - 2 * (vertex_id as i32 & 1)))
        + v * (mirror * (1 - 2 * (vertex_id as i32 >> 1)));
    let xyz_10 = (xyz.as_f32() + Vec3::ONE) / 2.0;
    let v = chunk_library.world_cells[chunk_id as usize]
        .truncate()
        .as_f32()
        + xyz_10;
    let p = v * CHUNK_SIZE as f32;
    *out_pos = global_data.world_to_projection * p.extend(1.0);
    *out_world_pos = p;
    *out_chunk_id = chunk_id;
}

#[spirv(fragment, unroll_loops)]
pub fn raytrace(
    #[spirv(frag_coord)] frag_coord: Vec4,
    #[spirv(push_constant)] global_data: &RayTracingGlobalData,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] library: &ChunkLibrary,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 1)] aux: &ChunkAuxData,
    #[spirv(descriptor_set = 0, binding = 2)] atlas_in: &RuntimeArray<AtlasIn>,
    in_world_pos: Vec3,
    hit_normal: &mut Vec4,
    #[spirv(flat)] hit_cell: &mut IVec4,
    hit_offset: &mut Vec4,
    z_value: &mut f32,
) {
    let ref material_atlas = unsafe { atlas_in.index(StandardAtlas::MaterialData as usize) };

    let zdir = global_data.inverse_direction;
    let direction = (zdir.xyz() - zdir.w * in_world_pos).normalize_or_zero();
    if direction.x == 0.0 && direction.y == 0.0 && direction.z == 0.0 {
        spirv_std::arch::kill();
    }

    use raytracing::*;

    let res = raytrace(
        &global_data,
        &library,
        &aux,
        in_world_pos,
        direction,
        frag_coord.z,
        1.0 / frag_coord.w,
        |mut hit_data, chunk| {
            let mat = chunk.fetch(hit_data.voxel, &material_atlas);
            handle_material(
                mat,
                hit_data.origin + hit_data.voxel,
                hit_data.entry,
                hit_data.exit,
                hit_data.length,
                hit_data.feature,
                &mut hit_data,
            )
        },
    );

    match res {
        RaytraceData {
            result: RaytraceResult::Miss,
            ..
        } => spirv_std::arch::kill(),
        RaytraceData {
            result: RaytraceResult::Hit,
            normal,
            cell,
            offset,
            depth,
            time: _,
        } => {
            *hit_normal = normal.extend(1.0);
            *hit_cell = cell.extend(0);
            *hit_offset = offset.extend(1.0);
            *z_value = depth;
        }
    }
}

fn handle_material(
    material_info: Vec4,
    tip: IVec3,
    entry_point: Vec3,
    _exit_point: Vec3,
    _time_inside: f32,
    _feature: BVec3,
    extra: &mut raytracing::HitData,
) -> bool {
    match material_info.x as i32 {
        0 => false,
        _ => {
            let center = tip.as_f32() + Vec3::splat(0.5);
            let delta = entry_point - center;
            let delta_feature = delta.cmpeq(Vec3::splat(delta.max_element()));
            let normal = Vec3::select(delta_feature, delta.signum(), Vec3::ZERO);
            extra.normal = normal;
            true
        }
    }
}

#[spirv(vertex)]
pub fn full_screen_quad(#[spirv(vertex_index)] vert_id: i32, out_pos: &mut Vec4) {
    *out_pos = vec4(
        ((vert_id / 2) * 2 - 1) as f32,
        (((vert_id % 2) ^ (vert_id / 2)) * 2 - 1) as f32,
        0.0,
        1.0,
    );
}
