#![no_std]
#![allow(incomplete_features)]
#![feature(asm, asm_experimental_arch, asm_const)]
#![cfg_attr(target_arch = "spirv", feature(register_attr), register_attr(spirv))]
pub mod generate;
pub mod preview;
pub mod sort;
