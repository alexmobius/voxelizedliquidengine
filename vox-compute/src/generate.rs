use spirv_std::RuntimeArray;

use core::ops::Range;
use vox_populi::all;
use vox_populi::chunks::CHUNK_SIZE;
use vox_populi::chunks::{
    atlas::{AtlasOut, SimpleChunkAccess},
    ChunkAuxData, ChunkLibrary, StandardAtlas,
};
use vox_populi::glam::{UVec3, Vec4};
#[spirv(compute(threads(16, 16)), unroll_loops)]
pub fn sponge(
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] library: &ChunkLibrary,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 1)] aux: &mut ChunkAuxData,
    #[spirv(descriptor_set = 0, binding = 3)] atlas_out: &RuntimeArray<AtlasOut>,
    #[spirv(workgroup_id)] wg_id: UVec3,
    #[spirv(local_invocation_id)] local_id: UVec3,
    #[spirv(push_constant)] range: &Range<u32>,
) {
    let coord = UVec3::new(local_id.x, local_id.y, wg_id.z);
    let chunk_id = range.start + wg_id.x;
    let atlas = unsafe { atlas_out.index(StandardAtlas::MaterialData as usize) };

    let chunk = library.get_chunk(chunk_id);
    aux.flags[chunk_id as usize].0 = 0;

    let fullcoord = coord.as_i32() + chunk.world_cell * CHUNK_SIZE as i32;
    let mut x = fullcoord.x.abs();
    let mut y = fullcoord.y.abs();
    let mut z = fullcoord.z;

    let mut material: usize;
    if x >= 0 && y >= 0 && z >= 0 {
        material = 1;
        for _ in 0..21 {
            let mods = [x % 3, y % 3, z % 3];
            let mut count = 0;
            for i in 0..3 {
                if mods[i] == 1 {
                    count += 1;
                }
            }
            if count > 1 {
                material = 0;
                break;
            }
            x = x / 3;
            y = y / 3;
            z = z / 3;
        }
    } else {
        material = 0;
    }

    chunk.write(
        coord.as_i32(),
        &atlas,
        Vec4::new(material as f32, 0.0, 0.0, 0.0),
    );
    if material != 0 {
        aux.flags[chunk_id as usize].set_visible(true);
    }
}
