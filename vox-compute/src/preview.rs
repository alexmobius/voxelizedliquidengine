use spirv_std::Image;

type NormalImage = Image!(format = rgba8_snorm, 2D, sampled);
type CellImage = Image!(format = rgba32i, 2D, sampled);
type OffsetImage = Image!(format = rgba8, 2D, sampled);
type ZBufferImage = Image!(format = r32f, 2D, sampled);
type DepthImage = Image!(type = f32, 2D, sampled, depth);

macro_rules! deferred_targets {
    () => {};
}
