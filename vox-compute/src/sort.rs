use spirv_std::arch::{
    all_memory_barrier_with_group_sync, workgroup_memory_barrier_with_group_sync,
};
use spirv_std::memory::{Scope, Semantics};
use spirv_std::RuntimeArray;

use vox_populi::chunks::{ChunkAuxData, ChunkLibrary, Flags, Ordering, CHUNK_SIZE, MAX_CHUNKS};
use vox_populi::glam::UVec3;
use vox_populi::particle::Particle;

const REDUCE_THREADS: u32 = 128;
const REDUCE_BLOCK_SIZE: u32 = REDUCE_THREADS * 2;
const LOG_REDUCE_BLOCK_SIZE: u32 = 8;
const CHUNK_EXTENT: u32 = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;
const PREFIX_SIZE: u32 = CHUNK_EXTENT * (MAX_CHUNKS as u32);

#[spirv(compute(threads(128)))]
pub fn apply_ordering(
    #[spirv(storage_buffer, descriptor_set = 0, binding = 1)] chunk_aux_data: &mut ChunkAuxData,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 0)] ordering: &Ordering,
    #[spirv(local_invocation_id)] id: UVec3,
) {
    const CHUNKS_PER_THREAD: usize = MAX_CHUNKS / (REDUCE_THREADS as usize);
    let idx = id.x * CHUNKS_PER_THREAD as u32;
    let mut values: [Flags; CHUNKS_PER_THREAD] = Default::default();

    for i in 0..CHUNKS_PER_THREAD {
        if idx + (i as u32) < ordering.length {
            values[i] = chunk_aux_data.flags[ordering.indices[idx as usize + i] as usize];
        }
    }

    unsafe { workgroup_memory_barrier_with_group_sync() };

    for i in 0..CHUNKS_PER_THREAD {
        if idx + (i as u32) < ordering.length {
            chunk_aux_data.flags[idx as usize + i] = values[i];
        }
    }
}

#[spirv(compute(threads(128)))]
pub fn count_particles(
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] chunk_data: &ChunkLibrary,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 0)] particles_in: &mut [Particle],
    #[spirv(global_invocation_id)] global_id: UVec3,
    #[spirv(storage_buffer, descriptor_set = 2, binding = 0)] out_counts: &mut [u32; PREFIX_SIZE
             as usize],
    #[spirv(push_constant)] input_size: &usize,
) {
    let idx = global_id.x as usize;
    if idx >= *input_size {
        return;
    }
    let cell = particles_in[idx].position().floor().as_i32();
    let chunk = vox_populi::chunks::voxel_to_chunk(cell);
    let offset = (cell - chunk * (CHUNK_SIZE as i32)).as_u32();

    if let Some(chunk_id) = chunk_data.find_chunk_index(chunk) {
        let offset_flat = ((offset.z * CHUNK_SIZE) + offset.y) * CHUNK_SIZE + offset.x;
        let offset_flat = offset_flat + chunk_id * CHUNK_EXTENT;
        let count_ptr = &mut out_counts[offset_flat as usize];
        let offset_ptr = &mut particles_in[idx].cell_offset;
        unsafe {
            asm! {
            "%u32 = OpTypeInt 32 0",
            "%relaxed = OpConstant %u32 {RELAXED}",
            "%workgroup = OpConstant %u32 {WORKGROUP}",
            "%result = OpAtomicIIncrement typeof*{count_ptr} {count_ptr} %workgroup %relaxed",
            "OpStore {offset_ptr} %result",
            RELAXED = const Semantics::NONE.bits() | Semantics::UNIFORM_MEMORY.bits(),
            WORKGROUP = const Scope::Workgroup as u32,
            count_ptr = in(reg) count_ptr,
            offset_ptr = in(reg) offset_ptr,
            }
        }
    }
}

use unroll::unroll_for_loops;
#[spirv(compute(threads(128)), unroll_loops)]
#[unroll_for_loops]
pub fn scan_reduce_counts(
    #[spirv(storage_buffer, descriptor_set = 2, binding = 0)] counts: &[u32],
    #[spirv(storage_buffer, descriptor_set = 2, binding = 1)] out_scan: &mut [u32],
    #[spirv(storage_buffer, descriptor_set = 2, binding = 2)] out_counts: &mut [u32],
    #[spirv(push_constant)] input_size: &u32,
    #[spirv(workgroup)] wg_block: &mut [u32; (REDUCE_BLOCK_SIZE as usize)],
    #[spirv(workgroup_id)] wg_id: UVec3,
    #[spirv(local_invocation_id)] local_id: UVec3,
) {
    let lidx = local_id.x;
    let idx = wg_id.x * REDUCE_BLOCK_SIZE + lidx;
    if idx < *input_size {
        wg_block[lidx as usize] = counts[idx as usize];
    }
    if idx + REDUCE_THREADS < *input_size {
        wg_block[(lidx + REDUCE_THREADS) as usize] = counts[(idx + REDUCE_THREADS) as usize];
    }

    let mut n = input_size - wg_id.x * REDUCE_BLOCK_SIZE;
    if n > REDUCE_BLOCK_SIZE {
        n = REDUCE_BLOCK_SIZE
    }
    let n = n as usize;

    for d in 0..LOG_REDUCE_BLOCK_SIZE {
        let step = 1u32 << d;
        let ai = ((lidx * step * 2) + step * 2 - 1) as usize;
        let bi = ((lidx * step * 2) + step - 1) as usize;
        if ai < n {
            wg_block[ai] += wg_block[bi];
        }
        unsafe { workgroup_memory_barrier_with_group_sync() };
    }

    let out = wg_block[n - 1];
    wg_block[n - 1] = 0;

    unsafe { workgroup_memory_barrier_with_group_sync() };

    for d in 0..LOG_REDUCE_BLOCK_SIZE {
        let d = LOG_REDUCE_BLOCK_SIZE - 1 - d;
        let step = 1u32 << d;
        let ai = ((lidx * step * 2) + step * 2 - 1) as usize;
        let bi = ((lidx * step * 2) + step - 1) as usize;
        if ai < n {
            let t = wg_block[ai];
            wg_block[bi] = wg_block[ai];
            wg_block[ai] += t;
        }

        unsafe { workgroup_memory_barrier_with_group_sync() };
    }

    if lidx == 0 {
        out_counts[wg_id.x as usize] = out;
    }

    if idx < *input_size {
        out_scan[idx as usize] = wg_block[lidx as usize]
    }
    if idx + REDUCE_THREADS < *input_size {
        out_scan[(idx + REDUCE_THREADS) as usize] = wg_block[(lidx + REDUCE_THREADS) as usize];
    }
}

#[spirv(compute(threads(128), entry_point_name = "sort_scan_fixup"))]
pub fn scan_fixup(
    #[spirv(storage_buffer, descriptor_set = 2, binding = 0)] counts: &[u32],
    #[spirv(storage_buffer, descriptor_set = 2, binding = 1)] out_scan: &mut [u32],
    #[spirv(push_constant)] input_size: &u32,
    #[spirv(workgroup_id)] wg_id: UVec3,
    #[spirv(local_invocation_id)] local_id: UVec3,
) {
    let lidx = local_id.x;
    let idx = wg_id.x * REDUCE_BLOCK_SIZE + lidx;
    let adj = counts[wg_id.x as usize];
    if idx < *input_size {
        out_scan[lidx as usize] += adj;
    }
    if idx + REDUCE_THREADS < *input_size {
        out_scan[(lidx + REDUCE_THREADS) as usize] += adj;
    }
}

#[spirv(compute(threads(128)))]
pub fn particle_sort(
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] chunk_data: &ChunkLibrary,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 0)] particles_in: &[Particle],
    #[spirv(storage_buffer, descriptor_set = 1, binding = 1)] particles_out: &mut [Particle],
    #[spirv(global_invocation_id)] global_id: UVec3,
    #[spirv(storage_buffer, descriptor_set = 2, binding = 1)] scan: &[u32; PREFIX_SIZE as usize],
    #[spirv(push_constant)] input_size: &usize,
) {
    let idx = global_id.x as usize;
    if idx >= *input_size {
        return;
    }
    let cell = particles_in[idx].position().floor().as_i32();
    let chunk = vox_populi::chunks::voxel_to_chunk(cell);
    let offset = (cell - chunk * (CHUNK_SIZE as i32)).as_u32();

    if let Some(chunk_id) = chunk_data.find_chunk_index(chunk) {
        let offset_flat = ((offset.z * CHUNK_SIZE) + offset.y) * CHUNK_SIZE + offset.x;
        let offset_flat = offset_flat + chunk_id * CHUNK_EXTENT;
        let cell_off = scan[offset_flat as usize];
        let total = particles_in[idx].cell_offset + cell_off;
        particles_out[total as usize] = particles_in[idx].clone();
    }
}
